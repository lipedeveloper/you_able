import 'package:dio/dio.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:you_able/app/app_widget.dart';
import 'package:you_able/app/app_bloc.dart';
import 'package:you_able/app/src/pages/classes/classes_bloc.dart';
import 'package:you_able/app/src/pages/login/login_bloc.dart';
import 'package:you_able/app/src/pages/login/login_repository.dart';
import 'package:you_able/app/src/shared/auth/auth_bloc.dart';
import 'package:you_able/app/src/shared/auth/auth_repository.dart';
import 'package:you_able/app/src/shared/custom_dio/custom_dio.dart';
import 'package:you_able/app/src/shared/drawer-menu/myDrawer_bloc.dart';

class AppModule extends ModuleWidget {
  @override
  List<Bloc> get blocs => [
        Bloc((i) => MyDrawerBloc()),
        Bloc((i) => ClassesBloc()),
        Bloc((i) => AppBloc()),
        Bloc((i) => LoginBloc()),
        Bloc((i) => AuthBloc()),
      ];

  @override
  List<Dependency> get dependencies => [
        Dependency((i) => Dio()),
        Dependency((i) => CustomDio()),
        Dependency((i) => AuthRepository()),
        Dependency((i) => LoginRepository()),
        Dependency((i) => MyDrawerBloc()),
      ];

  @override
  Widget get view => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
