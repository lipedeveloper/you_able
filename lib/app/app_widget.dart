import 'package:flutter/material.dart';
import 'package:you_able/app/src/pages/classes/classes_page.dart';
import 'package:you_able/app/src/pages/home/home_page.dart';
import 'package:you_able/app/src/pages/login/login_page.dart';


class AppWidget extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    '/Login': (context) => LoginPage(),
    '/Home': (context) => HomePage(),
    '/Classes': (context) => ClassesPage(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
      routes: routes,
    );
  }
}
