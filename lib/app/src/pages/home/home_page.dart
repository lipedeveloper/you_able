import 'package:flutter/material.dart';
import 'package:you_able/app/src/shared/drawer-menu/myDrawer.dart';

class HomePage extends StatefulWidget {
  static String tag = 'Home';

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Home"),
          backgroundColor: Colors.cyan,
        ),
        drawer: MyDrawer(),
        body: new Center(
          child: new Text("Home Page", style: new TextStyle(fontSize: 35.0)),
        ));
  }
}
