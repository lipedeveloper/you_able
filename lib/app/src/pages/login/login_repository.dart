import 'dart:convert';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:you_able/app/src/shared/custom_dio/custom_dio.dart';
import 'package:you_able/app/src/shared/models/login/login_request.dart';
import 'package:you_able/app/src/shared/models/login/login_response.dart';

class LoginRepository extends Disposable {
  final CustomDio dio = new CustomDio();

  Future<LoginResponse> createPost(Map<String, dynamic> data) async {
    try {
      var response = await dio.client.post("/Authentication", data: data);
      print(response);
      return LoginResponse.fromJson(response.data);
    } catch (e) {
      throw (e.message);
    }
  }

  Future<LoginResponse> passwordValidate(LoginRequest request) async {
    try {
      var dataEnconde = json.encode(request);
      var response =
          await dio.client.post("/Authentication", data: dataEnconde);
      var data = LoginResponse.fromJson(response.data[0]);
      return data;
    } catch (e) {
      var loginErr = new LoginResponse.fromError(false, e.message, true);
      return loginErr;
    }
  }

  @override
  void dispose() {}
}
