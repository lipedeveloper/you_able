import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:you_able/app/src/pages/login/login_repository.dart';
import 'package:you_able/app/src/shared/models/login/login_request.dart';
import 'package:you_able/app/src/shared/models/login/login_response.dart';
import 'package:you_able/app/src/shared/util/SharedPreferenesHelper.dart';

class LoginBloc extends BlocBase {
  final LoginRepository repo = LoginRepository();

  Future<LoginResponse> checkUserPassword(LoginRequest data) async {
    var resp = await repo.passwordValidate(data);

    StorageUtil.clear();
    StorageUtil.getInstance();
    StorageUtil.putObject('session-user', resp);
    StorageUtil.putObject('session', true);
    StorageUtil.putObject('facebook', false);

    return resp;
  }

  Future<bool> loginCacheFacebook(Map profileFacebook) async {
    StorageUtil.clear();
    StorageUtil.getInstance();
    StorageUtil.putObject('facebook-session', profileFacebook);
    StorageUtil.putObject('facebook', true);
    StorageUtil.putObject('session', true);

    return true;
  }

  @override
  void dispose() {
    super.dispose();
  }
}
