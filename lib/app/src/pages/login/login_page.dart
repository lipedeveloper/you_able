import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:http/http.dart' as http;
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:you_able/app/app_module.dart';
import 'dart:convert' as JSON;

import 'package:you_able/app/src/pages/login/login_bloc.dart';
import 'package:you_able/app/src/shared/models/login/login_response.dart';
import 'package:you_able/app/src/shared/util/custonsTools.dart';
import 'package:you_able/app/src/shared/validator/validator_form.dart';

import '../../shared/models/login/login_request.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final email = TextEditingController();
  final password = TextEditingController();
  Map userProfile;
  final facebookLogin = FacebookLogin();
  var bloc = AppModule.to.getBloc<LoginBloc>();
  var util = CustonsTools();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool isLoading = false;
  bool isDisabled = false;

  _loginWithFB() async {
    bool _isLoggedIn;
    final result = await facebookLogin.logInWithReadPermissions(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,picture,email,birthday&access_token=$token');
        final profile = JSON.jsonDecode(graphResponse.body);
        print(profile);
        setState(() {
          userProfile = profile;
          _isLoggedIn = true;
        });
        bloc.loginCacheFacebook(profile).then((response) =>
            {response ? Navigator.of(context).pushNamed('/Home') : null});
        break;

      case FacebookLoginStatus.cancelledByUser:
        setState(() => _isLoggedIn = false);
        break;
      case FacebookLoginStatus.error:
        setState(() => _isLoggedIn = false);
        break;
    }
    print(_isLoggedIn);
  }

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
      //    If all data are correct then save data to out variables
      _formKey.currentState.save();
    } else {
      //    If all data are not valid then start auto validation.
      setState(() {
        this._autoValidate = true;
      });
    }
  }

  login() async {
    try {
      setState(() {
        isLoading = true;
        isDisabled = true;
      });

      var data = new LoginRequest(
          email: this.email.text, password: this.password.text);

      var result = await bloc.checkUserPassword(data);
      var loginResponse = new LoginResponse();
      loginResponse = result;

      setState(() {
        isLoading = false;
      });

      if (loginResponse == null)
        util.ackAlert(context,
            "Problema de comunicação tente novamente mais tarde.", "Error");

      if (loginResponse.sucess) {
        Navigator.of(context).pushNamed('/Home');
      } else if (loginResponse.isError) {
        util.ackAlert(
            context,
            "Por favor Contate um administrador. \n\n Msg: " +
                loginResponse.msg,
            "Erro interno do Servidor");
      } else {
        util.ackAlert(context, loginResponse.msg, "Login");
      }
    } catch (er) {
      util.ackAlert(
          context,
          "Por favor Contate um administrador. \n\n Msg: " + er,
          "Erro interno do Servidor");
      setState(() {
        isLoading = false;
      });
    } finally {
      setState(() {
        isDisabled = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final logo = Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/bg-login.jpg'),
              fit: BoxFit.fitWidth,
              alignment: Alignment.topCenter)),
    );

    final email = Padding(
      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
      child: Container(
        color: Color(0xfff5f5f5),
        child: TextFormField(
          controller: this.email,
          validator: Validator().validateEmail,
          style: TextStyle(color: Colors.black, fontFamily: 'SFUIDisplay'),
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Username',
              prefixIcon: Icon(Icons.person_outline),
              labelStyle: TextStyle(fontSize: 15)),
        ),
      ),
    );

    final password = Container(
      color: Color(0xfff5f5f5),
      child: TextFormField(
        controller: this.password,
        validator: Validator().validatePassword,
        obscureText: true,
        style: TextStyle(color: Colors.black, fontFamily: 'SFUIDisplay'),
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Password',
            prefixIcon: Icon(Icons.lock_outline),
            labelStyle: TextStyle(fontSize: 15)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(),
        disabledColor: Colors.grey,
        onPressed: isDisabled
            ? null
            : () {
                this._validateInputs();
                _formKey.currentState;
                if (_formKey.currentState.validate()) {
                  this.login();
                }
              },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Log In', style: TextStyle(color: Colors.white)),
      ),
    );

    final loginButtonFB = SignInButton(
      Buttons.Facebook,
      text: "Continue with Facebook",
      onPressed: () {
        _loginWithFB();
      },
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );

    Widget _buildWidget() {
      return Scaffold(
        body: Stack(children: <Widget>[
          logo,
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 230),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.only(left: 24.0, right: 24.0),
                  children: <Widget>[
                    SizedBox(height: 28.0),
                    email,
                    SizedBox(height: 8.0),
                    password,
                    SizedBox(height: 24.0),
                    loginButton,
                    loginButtonFB,
                    forgotLabel
                  ],
                ),
              ))
        ]),
      );
    }

    return new Scaffold(
      body: ModalProgressHUD(child: _buildWidget(), inAsyncCall: isLoading),
    );
  }
}
