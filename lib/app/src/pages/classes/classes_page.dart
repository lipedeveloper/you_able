import 'package:flutter/material.dart';
import 'package:you_able/app/app_module.dart';
import 'package:you_able/app/src/pages/classes/classes_bloc.dart';
import 'package:you_able/app/src/pages/classes/components/GridViewDetailPage.dart';
import 'package:you_able/app/src/shared/constants.dart';
import 'package:you_able/app/src/shared/drawer-menu/myDrawer.dart';
import 'package:you_able/app/src/shared/models/classes/move_data.dart';

class ClassesPage extends StatefulWidget {
  @override
  _ClassesPageState createState() => _ClassesPageState();
}

class _ClassesPageState extends State<ClassesPage> {
  var bloc = AppModule.to.getBloc<ClassesBloc>();

  Widget _backImage(MoveItem item) {
    return AspectRatio(
      aspectRatio: 1.5,
      child: Image.asset(
        item.trailerImg1,
        fit: BoxFit.fill,
      ),
    );
  }

  Widget _cardBottom(MoveItem item) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 10.0,
        ),
        Text(
          item.name,
          style: TextStyle(
            color: Colors.grey[900],
            fontSize: TEXT_LARGE_SIZE,
            fontWeight: FontWeight.w700,
          ),
        ),
        SizedBox(height: 5.0),
        Text(
          item.category,
          style: TextStyle(color: Colors.grey[500], fontSize: TEXT_SMALL_SIZE),
        ),
        SizedBox(width: 6.0),
        Row(
          children: <Widget>[
            SizedBox(width: 6.0),
          ],
        )
      ],
    );
  }

  Widget _grdItem(MoveItem item) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => GridViewDetailPage(item: item)));
      },
      child: Card(
        elevation: 1.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _backImage(item),
            Container(
              padding: EdgeInsets.only(left: 4.0, right: 4.0, bottom: 6.0),
              child: _cardBottom(item),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var items = bloc.getItemList();
    var l = List<Widget>();
    for (var item in items) {
      l.add(_grdItem(item));
    }
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Classes"),
        backgroundColor: Colors.cyan,
      ),
      drawer: MyDrawer(),
      body: Container(
        child: ListView(
          children: l,
        ),
      ),
    );
  }
}
