import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:you_able/app/src/shared/models/classes/move_data.dart';

class ClassesBloc extends BlocBase {
  List<MoveItem> getItemList() {
    return [
      MoveItem(
        id: 0,
        name: 'Video teste 1',
        category: 'Teste SubTitulo',
        desc: 'The Avengers and their allies must be willing '
            'to sacrifice all in an attempt to defeat '
            'the powerful Thanos before his blitz of '
            'devastation and ruin puts an end to the universe.'
            '\nAs the Avengers and their allies have continued '
            'to protect the world from threats too large for '
            'any one hero to handle, a danger has emerged '
            'from the cosmic shadows: Thanos.',
        rating: 8.7,
        directors: 'Directors: Anthony Russo, Joe Russo',
        releaseDate: '27 April 2018',
        releaseDateDesc: 'USA (2018), 2h 29min',
        runtime: '2h 29min',
        bannerUrl: 'assets/bg-login.jpg',
        imageUrl: 'assets/bg-login.jpg',
        trailerImg1: 'assets/bg-login.jpg',
        trailerImg2: 'assets/bg-login.jpg',
        trailerImg3: 'assets/bg-login.jpg',
      ),
      MoveItem(
        id: 1,
        name: 'Video Teste 2',
        category: 'Teste SubTitulo',
        desc: 'Autobots and Decepticons are at war, with humans '
            'on the sidelines. Optimus Prime is gone. The key to '
            'saving our future lies buried in the secrets of the past, '
            'in the hidden history of Transformers on Earth.',
        rating: 5.2,
        directors: 'Directors: Michael Bay',
        releaseDate: '21 June 2017',
        releaseDateDesc: 'USA (2017), 2h 34min',
        runtime: '2h 34min',
        bannerUrl: 'assets/bg-menu.jpg',
        imageUrl: 'assets/bg-menu.jpg',
        trailerImg1: 'assets/bg-menu.jpg',
        trailerImg2: 'assets/bg-menu.jpg',
        trailerImg3: 'assets/bg-menu.jpg',
      ),
      MoveItem(
        id: 3,
        name: 'Video teste 3',
        category: 'Teste SubTitulo',
        desc: 'The Avengers and their allies must be willing '
            'to sacrifice all in an attempt to defeat '
            'the powerful Thanos before his blitz of '
            'devastation and ruin puts an end to the universe.'
            '\nAs the Avengers and their allies have continued '
            'to protect the world from threats too large for '
            'any one hero to handle, a danger has emerged '
            'from the cosmic shadows: Thanos.',
        rating: 8.7,
        directors: 'Directors: Anthony Russo, Joe Russo',
        releaseDate: '27 April 2018',
        releaseDateDesc: 'USA (2018), 2h 29min',
        runtime: '2h 29min',
        bannerUrl: 'assets/bg-login.jpg',
        imageUrl: 'assets/bg-login.jpg',
        trailerImg1: 'assets/bg-login.jpg',
        trailerImg2: 'assets/bg-login.jpg',
        trailerImg3: 'assets/bg-login.jpg',
      ),
    ];
  }

  @override
  void dispose() {
    super.dispose();
  }
}
