import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:you_able/app/src/shared/util/SharedPreferenesHelper.dart';
import 'package:you_able/app/src/shared/util/custonsTools.dart';

class MyDrawerBloc extends BlocBase {
  dynamic getProfile(String key) {
    // var loggedCache = CustonsTools().localStorage('session', 'logged');
    // var facebookCache = CustonsTools().localStorage('session', 'facebook');

    StorageUtil.getInstance();
    var loggedCache = StorageUtil.getObject('session');
    var facebookCache = StorageUtil.getObject('facebook');

    bool logged = loggedCache != null ? loggedCache : false;
    bool facebook = facebookCache != null ? facebookCache : false;

    if (logged && !facebook) {
      var t = StorageUtil.getObject('session-user')[key];
      return dataCache(t, facebook, key);
    } else if (logged && facebook) {
      var t = StorageUtil.getObject('facebook-session')[key];
      return dataCache(t, facebook, key);
    }
  }

  dynamic dataCache(dynamic item, bool facebook, String key) {
    if (facebook && key == 'picture') {
      return item["data"]["url"];
    } else {
      return item;
    }
  }

  void logout() {
    CustonsTools().clearStorage('session');
  }

  //dispose will be called automatically by closing its streams
  @override
  void dispose() {
    super.dispose();
  }
}
