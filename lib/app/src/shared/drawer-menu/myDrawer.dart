import 'package:flutter/material.dart';
import 'package:you_able/app/app_module.dart';
import 'package:you_able/app/src/shared/drawer-menu/myDrawer_bloc.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  var bloc = AppModule.to.getBloc<MyDrawerBloc>();

  @override
  Widget build(BuildContext context) {
    String name = bloc.getProfile('name');
    String email = bloc.getProfile('email');
    String picture = bloc.getProfile('picture');

    return new Drawer(
      child: new ListView(
        children: <Widget>[
          new UserAccountsDrawerHeader(
              accountEmail: new Text(email),
              accountName: new Text(name),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                  backgroundImage: new NetworkImage(picture),
                ),
              ),
              otherAccountsPictures: <Widget>[
                new GestureDetector(
                    child: new Icon(Icons.exit_to_app, color: Colors.white),
                    onTap: () => {
                          bloc.logout(),
                          Navigator.of(context).pop(),
                          Navigator.of(context).pushNamed('/Login')
                        }),
              ],
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                image: new AssetImage("assets/bg-menu.jpg"),
                fit: BoxFit.cover,
              ))),
          new Divider(),
          new ListTile(
              title: new Text("Home"),
              trailing: new Icon(Icons.info),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed('/Home');
              }),
          new ListTile(
              title: new Text("Classes"),
              trailing: new Icon(Icons.book),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamed('/Classes');
              }),
        ],
      ),
    );
  }
}
