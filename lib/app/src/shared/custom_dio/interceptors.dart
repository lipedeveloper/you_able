import 'package:dio/dio.dart';

class CustomIntercetors extends InterceptorsWrapper {
  @override
  onRequest(RequestOptions options) async {
    print("REQUEST[${options.method}] => PATH: ${options.path}");
  }

  @override
  onResponse(Response response) async {
    //200
    //201
    print("RESPONSE[${response.statusCode}] => PATH: ${response.request.path}");
  }

  @override
  onError(DioError e) async {
    //Exception
    print("ERROR[${e.response?.statusCode}] => PATH: ${e.request.path}");
  }
}
