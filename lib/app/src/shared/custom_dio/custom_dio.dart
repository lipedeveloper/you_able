import 'package:dio/dio.dart';
import 'package:you_able/app/src/shared/constants.dart';
import 'package:you_able/app/src/shared/custom_dio/interceptor_cache.dart';
import 'package:you_able/app/src/shared/custom_dio/interceptors.dart';

 

class CustomDio {
  final Dio client = new Dio(BaseOptions(
    contentType: Headers.jsonContentType
  ));

  CustomDio() {
     client.options.baseUrl = BASE_URL;
     client.interceptors.add(CacheIntercetors());
     client.interceptors.add(CustomIntercetors());
    // client.interceptors.add(AuthIntercetors());
    client.options.connectTimeout = 5000;
  }
}
