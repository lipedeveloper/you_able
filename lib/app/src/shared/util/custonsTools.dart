import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustonsTools {
  Future<void> ackAlert(BuildContext context, String message, String title) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> setCacheObject(String key, dynamic obj) async {
    final prefs = await SharedPreferences.getInstance();

    prefs.setString(key, jsonEncode(obj));
  }

  Future<dynamic> getCacheObject(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var sharedPreferences = jsonDecode(prefs.getString(key));
    return sharedPreferences;
  }

  Future<void> setCacheList(String key, List<String> value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList(key, value);
  }

  Future<void> setCache(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  Future<String> getCacheByString(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  Future<dynamic> getCacheDynamic(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.get(key);
  }

  dynamic localStorage(String storageName, String key) {
    final LocalStorage storage = new LocalStorage(storageName);
    return storage.getItem(key);
  }

  dynamic localStorageObject(String storageName) {
    final LocalStorage storage = new LocalStorage(storageName);
    return storage;
  }

  void clearStorage(String storageName) {
    final LocalStorage storage = new LocalStorage(storageName);
    storage.clear();
  }
}
