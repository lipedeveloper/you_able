class BaseResponse {
  bool sucess;
  String msg;
  bool isError;

  BaseResponse({this.sucess, this.msg, this.isError});

  BaseResponse.fromJson(Map<String, dynamic> json) {
    sucess = json['sucess'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sucess'] = this.sucess;
    data['msg'] = this.msg;
    return data;
  }
}
