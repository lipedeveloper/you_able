class PostModel {
  bool sucess;
  String msg;

  PostModel({this.sucess, this.msg});

  PostModel.fromJson(Map<String, dynamic> json) {
    sucess = json['sucess'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sucess'] = this.sucess;
    data['msg'] = this.msg;
    return data;
  }
}
