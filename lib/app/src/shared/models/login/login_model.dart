import 'package:you_able/app/src/shared/models/base_response.dart';

class LoginModel extends BaseResponse {
  String email;
  String password;
  String name;
  String picture;
  DateTime birthday;

  LoginModel({this.email, this.password, this.picture, this.birthday});

  LoginModel.fromLoginModel(LoginModel loginModel) {
    this.email = loginModel.email;
    this.password = loginModel.password;
    this.name = loginModel.name;
    this.picture = loginModel.picture;
    this.birthday = loginModel.birthday;
  }

  LoginModel.fromJson(Map<String, dynamic> json)
      : super(msg: json['msg'], sucess: json['sucess']) {
    var userInfo = json['userinfo'];
    if (userInfo != null) {
      this.email = userInfo['email'];
      this.name = userInfo['name'];
      this.picture = userInfo['picture'];
      this.birthday = userInfo['birthday'];
    }
  }

  LoginModel.fromError(String msg, bool sucess, bool isError)
      : super(msg: msg, sucess: sucess, isError: isError) {
    this.sucess = sucess;
    this.msg = msg;
    this.isError = isError;
  }
}
