import 'package:you_able/app/src/shared/models/base_response.dart';

class LoginResponse extends BaseResponse {
  UserInfo userInfo;
  bool isError = false;

  LoginResponse({this.userInfo});

  LoginResponse.fromJson(Map<String, dynamic> json)
      : super(sucess: json['sucess'], msg: json['message']) {
    this.userInfo = json['userInfo'] != null
        ? new UserInfo.fromJson(json['userInfo'])
        : null;
  }

  LoginResponse.fromError(bool sucess, String msg, [bool isError = false])
      : super(sucess: sucess, msg: msg) {
    this.isError = isError;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sucess'] = super.sucess;
    data['message'] = super.msg;
    if (this.userInfo != null) {
      data['userInfo'] = this.userInfo.toJson();
    }
    return data;
  }
}

class UserInfo {
  String email;
  String name;
  String picture;
  String birthDay;

  UserInfo({this.email, this.name, this.picture, this.birthDay});

  UserInfo.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    name = json['name'];
    picture = json['picture'];
    birthDay = json['birthDay'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['name'] = this.name;
    data['picture'] = this.picture;
    data['birthDay'] = this.birthDay;
    return data;
  }
}
