const BASE_URL = "http://54.201.201.80/yaAPI";

//Const for movies
const TEXT_HUGE_SIZE = 100.0;
const TEXT_LARGE_SIZE = 24.0;
const TEXT_NORMAL_SIZE = 16.0;
const TEXT_SMALL_SIZE = 10.0;
const TEXT_SMALL_SIZE_2 = 8.0;

const LINE_SMALL = 1.0;
const LINE_NORMAL = 10.0;

const SELECTOR_ONE_HEIGHT = 80.0;
const SELECTOR_TWO_HEIGHT = 150.0;
const RELEASE_DATE = "RELEASE DATE", RUNTIME = "RUMTIME";
const GRID_VIEW_NAME = "Classes";

//end const for movies
